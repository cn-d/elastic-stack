#!/bin/bash

until curl -s "http://elasticsearch:9200" > /dev/null; do 
    echo "Waiting for Elasticsearch..."
    sleep 1
done

curl -XPUT -H "Content-Type: application/json" http://elasticsearch:9200/_ingest/pipeline/game_log --data-binary @game_log.json
curl -XPUT -H "Content-Type: application/json" http://elasticsearch:9200/_ingest/pipeline/service_log --data-binary @service_log.json