#!/bin/bash

set -euo pipefail

beat=$1

until curl -s "http://elasticsearch:9200" > /dev/null; do 
    echo "Waiting for Elasticsearch..."
    sleep 1
done

until curl -s "http://kibana:5601" > /dev/null; do
    echo "Waiting for kibana..."
    sleep 1
done

# Kibana takes time to become healthy when URL is available
sleep 15

${beat} --strict.perms=false -e